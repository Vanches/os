using System;


/// <summary>
/// Класс User, который имплиментит интерфей UserPass и передает ему тип UserPass
/// </summary>
 public class UserPass : User, IUserPass<UserPass>
{/// <summary>
/// The pass.
/// </summary>
	private String pass;
	/// <summary>
	/// Initializes a new instance of the <see cref="UserPass"/> class.
	/// </summary>
	/// <param name="id">Identifier.</param>
	/// <param name="login">Login.</param>
	/// <param name="pass">Pass.</param>
	public UserPass(int id, String login, String pass)
		:base(id,login){
		this.pass = pass;

	}
	/// <summary>
	/// Bases the user.
	/// </summary>
	/// <returns>The user.</returns>
	public UserPass BaseUser(){

		UserPass user = new UserPass (1, "admin", "root");
		return user;
	}
	/// <summary>
	/// Infos the user.
	/// </summary>
	/// <param name="obj">Object.</param>
	public override void infoUser (User obj)
	{
		base.infoUser (obj);
		Console.WriteLine ("Pass: {0}", pass);
	}

}