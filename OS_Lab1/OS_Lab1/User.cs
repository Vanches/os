using System;

/// <summary>
/// Класс User, который имплиментит интерфей <cref="IserInfo"/> и передает ему тип User
/// </summary>
public class User : IUserInfo<User>
{
	private String login;
	private int id;
	/// <summary>
	/// Initializes a new instance of the <see cref="User"/> class.
	/// </summary>
	/// <param name="id">Identifier.</param>
	/// <param name="login">Login.</param>
	public User(int id,String login){
		this.login = login;
		this.id = id;
	}
	/// <summary>
	/// Login this instance.
	/// </summary>
	public String Login(){
		return login;
	}/// <summary>
	/// Identifier this instance.
	/// </summary>
	public int Id(){
		return id;
	}
	/// <summary>
	/// Infos the user.
	/// </summary>
	/// <param name="obj">Object.</param>
	public virtual void infoUser(User obj){
		Console.WriteLine (
			@"
Информация о пользователе:
ID: {0}
Login: {1}
",id,login);

	}

}