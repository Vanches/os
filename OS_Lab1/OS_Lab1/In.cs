using System;
/// <summary>
/// I user.
/// </summary>

public interface IUser
{
	/// <summary>
	/// Login this instance.
	/// </summary>
	String Login ();
	/// <summary>
	/// Identifier this instance.
	/// </summary>
	int Id();
}
/// <summary>
/// Принимет Обобщенный тип, который ограничен  наследованием от интерфейса IUser
/// </summary>
public interface IUserPass<out T> :IUser
{
	/// <summary>
	/// Bases the user.
	/// </summary>
	/// <returns>The user.</returns>
	T BaseUser();
}
public interface IUserInfo<in T> : IUser
{
	/// <summary>
	/// Infos the user.
	/// </summary>
	/// <param name="obj">Object.</param>
		void infoUser(T obj);
}






